<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "Controller@index");

Route::get('/profil', "Controller@profil");
Route::get('/registrer', "Controller@registrer");
Route::get('/add', "Controller@add");
Route::get('/post','Controller@post' );
Route::get('/connection','Controller@connection');
Route::get('/logout','UserController@logoutUser' );



Route::post('user_store', ['uses' => 'UserController@store']);
Route::post('user_registrer', ['uses' => 'UserController@registrerUser']);
Route::post('post_create', ['uses' => 'PostController@create']);


Route::resource('/requests', 'RequestController');
Route::get('/deals', "DealController@index");


