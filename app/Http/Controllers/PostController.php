<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Session;

class postController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //Login user and redirect to login view
        $add = $request->get('add', 0);
        $title = $request->input('title');
        $desc = $request->input('desc');
        $price = $request->input('price');
        $instrument = $request->input('instrument');
        $idUser = Session()->get('user.id');
        date_default_timezone_set('Europe/Paris');
        $date = date("Y-m-d H:i");

        if($add == "deals"){
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'desc' => 'required',
                'price' => 'required|integer',
            ]);
        }
        else{
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'desc' => 'required',
                'price' => 'integer',
            ]);
        }

        if($validator->fails()){

            return redirect('add')->withInput()->withErrors($validator->errors());
        }
        else{

            $data['add'] = $add;
            $data['title'] = $title;
            $data['desc'] = $desc;
            $data['price'] = $price;
            $data['instrument'] = $instrument;
            $data['idUser'] = $idUser;
            $data['date'] = $date;

            $adds = ucfirst($add);
            $titleI = "title".$adds;
            $descI = "desc".$adds;
            $dateI = "date".$adds;
            $priceI = "price".$adds;
            $instrumentI = "instrument".$adds;
            $idUserI = "user".$adds;
            $idAddI = "id".$adds;



            //DB::insert("insert into $add ($titleI, $descI, $dateI, $priceI, $idUserI, $instrumentI)
            //values (?, ?, ?, ?, ?, ?)", array($title, $desc, $date, $price, $idUser, $instrument));

            $idAdd = DB::select("select max($idAddI) from $adds where $idUserI = ?", [$idUser]);

            return redirect('post')->with('idAdd',$idAdd);

        }



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
