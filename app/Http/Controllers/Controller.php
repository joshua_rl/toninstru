<?php

namespace App\Http\Controllers;

use Cassandra\Date;
use DateTime;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //Redirect to index page
    public function index () {
        return view('about.index');
    }

    //Redirect to deals page
    public function deals () {
        return view('about.deals');
    }

    //Redirect to login page
    public function profil () {
        $id = session()->get('user.id');
        $deals = DB::select('SELECT * FROM deals where userDeals = ?', [$id] );
        $requests = DB::select('SELECT * FROM request where userRequest = ?', [$id] );
        $post = array();
        $post["deals"] = array();
        $post["requests"] = array();
        if($deals == null){
            $post['messageDeals'] = "Aucune Offre";
        }
        if($requests == null){
            $post['messageRequests'] = "Aucune Demande";
        }
        foreach ($deals as $deal){
                $instrument = $this->getInstruments($deal->instrumentDeals);
                $deal->instrumentDeals = $instrument;
                $date = $this->getDate($deal->dateDeals);
                $deal->dateDeals = $this->getDate($deal->dateDeals);
                array_push($post['deals'], $deal );
        }
        foreach ($requests as $request ){
                $instrument = $this->getInstruments($request->instrumentRequest);
                $request->instrumentRequest = $instrument;
                $date = $this->getDate($request->dateRequest);
                $request->dateRequest = $date;
                array_push($post['requests'], $request );
        }


        return view('about.profil', ['post' => $post]);
    }

    //Redirect to subscribe page
    public function registrer () {
        return view('about.registrer');
    }

    //Redirect to requests page
    public function requests () {
        return view('about.requests');
    }

    //Redirect to add page with instruments
    public function add () {
        $requests = DB::select('SELECT * FROM instrument');
        return view('about.add', ["instruments" => $requests]);
    }

    //Redirect to post page
    public function post () {
        return view('about.post');
    }

    //Redirect to connection page
    public function connection () {
        return view('about.connection');
    }

    public function getInstruments($id){
        $requests = DB::select('SELECT wordingInstrument FROM instrument where idInstrument = ?', [$id]);
        return $requests[0]->wordingInstrument;
    }

    public function getDate($dateTime){
        setlocale(LC_TIME, "fr_FR");
        $d = strftime("%a %e, %b %R", strtotime($dateTime));
        return $d;

    }
}
