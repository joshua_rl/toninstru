<?php

namespace App\Http\Controllers;

use phpDocumentor\Reflection\Type;
use PhpParser\Node\Expr\Cast\Object_;
use PhpParser\Node\Scalar\String_;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Redirection if form is completed
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {

            //Redirect to connection page with error message
            return redirect('connection')->withErrors($validator->errors());
        }
        else{

            //Login user and redirect to login view
            $email = $request->input('email');
            $password = $request->input('password');
            $users = $this->loginUser($email, $password);

            if ($users != null) {

                return redirect('profil');
            }
            else{

                //Display error message
                $errorDb = "Adresse email ou mot de passe invalide.";
                return view('about.connection', ['errorDb' => $errorDb]);
            }
        }
    }

    public function loginUser(String $email, String $password)
    {
        //Created session User
        $users = DB::select("select * from user where emailUser = ? and passwordUser = ?", [$email, $password]);

        if ($users != null){

            $user = $users[0];
            Session::put('user.id', $user->idUser);
            Session::put('user.lastName', $user->lastNameUser);
            Session::put('user.firstName', $user->firstNameUser);
            Session::put('user.pseudo', $user->pseudoUser);
            Session::put('user.email', $user->emailUser);
            Session::put('user.city', $user->cityUser);
            Session::put('user.postalCode', $user->pcUser);
            Session::put('user.phone', $user->phoneUser);
            Session::put('user.password', $user->passwordUser);
            Session::save();
        }

        return $users;
    }

    public function logoutUser(){

        Session::forget('user');
        return redirect('deals');
    }

    public function registrerUser(Request $request)
    {

        //Get input registrer
        $lastname = $request->input('lastName');
        $firstname = $request->input('firstName');
        $pseudo = $request->input('pseudo');
        $city = $request->input('city');
        $postalCode = $request->input('postalCode');
        $phone = $request->input('phone');
        $email = $request->input('email');
        $password = $request->input('password');

        $validator = Validator::make($request->all(), [

            //Input restrictions
            'lastName' => 'required|String|max:30',
            'firstName' => 'required|String|max:30',
            'pseudo' => "required|max:15",
            'city' => 'required|max:30',
            'postalCode' => 'required|Numeric|digits_between: 5, 10',
            'email' => 'required|email|max:50',
            'phone' => 'numeric|digits_between:10, 13',
            'password' => 'required|confirmed|max:50',
            'password_confirmation' => 'required',
        ]);

        $validator->after(function($validator) use ($email, $pseudo) {

            if ($this->userExists("pseudoUser", $pseudo)){

                //Display error message
                $validator->errors()->add("pseudo", "Le pseudo existe déjà.");
            }

            if ($this->userExists("emailUser", $email)){

                //Display error message
                $validator->errors()->add("email", "L'adresse e-mail existe déjà.");
            }
        });

        if ($validator->fails()) {

            //Redirect to connection page with error message
            return redirect('registrer')->withInput()->withErrors($validator->errors());
        }
        else {

            //Create new user on database
            DB::insert('insert into user (lastNameUser, firstNameUser, pseudoUser, cityUser, pcUser, emailUser, phoneUser, passwordUser)
            values (?, ?, ?, ?, ?, ?, ?, ?)', array($lastname, $firstname, $pseudo, $city, $postalCode, $email, $phone, $password));

            //Login user
            $this->loginUser($email, $password);
            return redirect('profil');
        }
    }

    public function userExists($attribute, $value)
    {
        $user = DB::select("SELECT idUser FROM user WHERE $attribute = ?", [$value]);
        if($user == null){
            $res = false;
        }
        else{
            $res = true;
        }
        return $res;
    }

    public function getUser(Int $id){

        $user = DB::select('select lastName, firstName, pseudoUser, cityUser, pcUser, emailUser, phoneUser from user where idUser = ?', [$id]);
        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
