<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-compatible" content="IE-edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name', 'Toninstru')}}</title>

        <!-- Link Css width bootstrap css-->
        <link href="{{ asset('/css/bootstrap.min.css') }}" rel ="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css?v=1') }}">

        <!-- Link Police Icons -->
        <link rel="stylesheet" href="{{url('https://use.fontawesome.com/releases/v5.2.0/css/all.css')}}" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

        <!-- Link Script -->
        <script src="{{url('https://code.jquery.com/jquery-3.4.1.js')}}" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/js/fonction.js') }}"></script>
    </head>
    <body>
    <!-- Implement header -->
        <header class="header">
            @include('inc.header')
        </header>

        <!-- Implement nav bar -->
        <nav class="menu">
            @include('inc.navbar')
        </nav>

        <!-- Implement content -->
    <div class="content">
        @yield('content')
    </div>

    </body>
</html>