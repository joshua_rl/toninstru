@extends('layouts.app')

@section('content')
    <div class="page-title">Connexion</div>
    <div class="page-connection">

        <!-- Display error message from form -->
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!-- Display error message if email or password invalid -->
        @if(isset($errorDb))
            <div class="alert alert-danger">
                <ul>
                        <li>{{$errorDb}}</li>
                </ul>
            </div>
        @endif

        {!! Form::open([ 'action'=>'UserController@store', 'method'=>'POST', 'class'=>'c-connection']) !!}
            {!! Form::label('l-email', 'Adresse e-mail', array('class' => 'c-label')) !!}
            {!! Form::text('email','', array('class' => 'c-mail','placeholder'=>'xyz@example.com')) !!}
            {!! Form::label('l-password', 'Mot de passe', array('class' => 'c-label')) !!}
            {!! Form::password('password', array('class' => 'c-pass','placeholder'=>'Mot de passe')) !!}
            {!! Form::submit('Connexion', array('class' => 'c-sub')) !!}
            <a class="btn-sub" href="{{ url('registrer') }}">S'inscrire</a>
        {!! Form::close() !!}


    </div>
@endsection