@extends('layouts.app')

@section('content')
    @if(session()->has('user.id'))
        <div class="page-title">Nouvelle Annonce</div>
        <div class="c-add add">

            {!! Form::open(['id'=>"f-add", 'action'=>'PostController@create', 'method'=>'POST', 'class'=>'f-connection']) !!}

            <!-- Deals or Request -->

                <div class="btn-group btn-group-toggle r-add" data-toggle="buttons">
                    <label class="btn btn-secondary active">
                        {!! Form::radio('add', 'deals', true, array('class' => 'a-radio')) !!} Offre
                    </label>
                    <label class="btn btn-secondary">
                        {!! Form::radio('add', 'request', false, array('class' => 'a-radio')) !!} Demande
                    </label>
                </div>
                <br/>

            <select class="a-select" name="instrument" form="f-add">
                <option class="l-instrument" value="none">-- Instrument --</option>
                @foreach($instruments as $instrument)
                    <option class="l-instrument" value="{{$instrument->idInstrument}}">{{$instrument->wordingInstrument}}</option>
                @endforeach
            </select>

            {!! Form::label('a-title', "Titre de l'annonce*", array('class' => 'a-label')) !!}
            {!! Form::text('title', old('title'), array('class' => 'a-title')) !!}

            <!-- Display error message -->
            @if($errors->has('title'))
                <div class="alert-danger">
                    <ul>
                        <li>{{$errors->first('title')}}</li>
                    </ul>
                </div>
            @endif

            {!! Form::label('a-desc', "Description de l'annonce*", array('class' => 'a-label')) !!}
            {!! Form::textarea('desc', old('desc'), array('class' => 'a-desc')) !!}

                <!-- Display error message -->
                @if($errors->has('desc'))
                    <div class="alert-danger">
                        <ul>
                            <li>{{$errors->first('desc')}}</li>
                        </ul>
                    </div>
                @endif

            {!! Form::label('a-inst', "Prix*", array('class' => 'a-label l-price')) !!}
            {!! Form::text('price', old('price'), array('class' => 'a-price ')) !!} €

            <!-- Display error message -->
            @if($errors->has('price'))
                <div class="alert-danger">
                    <ul>
                        <li>{{$errors->first('price')}}</li>
                    </ul>
                </div>
            @endif

            <!-- Submit -->
            {!! Form::submit("Publier", array('class' => 'r-sub a-sub')) !!}

            {!! Form::close() !!}
        </div>
    @else
        <script>
            window.location.href = '{{url("connection")}}';
        </script>
    @endif


@endsection