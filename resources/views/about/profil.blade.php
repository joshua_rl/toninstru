@extends('layouts.app')

@section('content')
    @if(session()->has('user.id'))
        <div class="page-title">Profil</div>

        <div class="row page-profil">
            <div class="col-3">
                <div class="nav flex-column nav-pills nav-profil" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-deals-tab" data-toggle="pill" href="#v-pills-deals" role="tab" aria-controls="v-pills-deals" aria-selected="true">Offres</a>
                    <a class="nav-link" id="v-pills-requests-tab" data-toggle="pill" href="#v-pills-requests" role="tab" aria-controls="v-pills-requests" aria-selected="false">Demandes</a>
                    <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Messages</a>
                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Paramètres</a>
                </div>
            </div>
            <div class="col-9">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade content-profil show active " id="v-pills-deals" role="tabpanel" aria-labelledby="v-pills-deals-tab">
                        <div class="sub-title">Offres</div>

                        @foreach($post["deals"] as $deal)
                            <div class="post">
                                <div class="p-title">{{$deal->titleDeals}}</div>
                                <div class="p-desc">{{$deal->descDeals}}</div>
                                <div class="p-date">{{$deal->dateDeals}}</div>
                                <div class="p-price">Pirx : {{$deal->priceDeals}} €</div>
                                <div class="p-instr">Catégorie : {{$deal->instrumentDeals}}</div>
                            </div>
                            <br />
                        @endforeach
                        @if(isset($post['messageDeals']))
                            <div class="p-message ">{{$post['messageDeals']}}</div>
                        @endif
                    </div>
                    <div class="tab-pane fade content-profil" id="v-pills-requests" role="tabpanel" aria-labelledby="v-pills-requests-tab">
                        <div class="sub-title">Demandes</div>
                        @foreach($post["requests"] as $request)
                            <div class="post">
                                <div class="p-title">{{$request->titleRequest}}</div>
                                <div class="p-desc">{{$request->descRequest}}</div>
                                <div class="p-date">{{$request->dateRequest}}</div>
                                <div class="p-price">Prix : {{$request->priceRequest}} €</div>
                                <div class="p-instr">Catégorie : {{$request->instrumentRequest}}</div>
                            </div>
                            <br />
                        @endforeach
                        @if(isset($post['messageRequests']))
                            <div class="p-message ">{{$post['messageRequests']}}</div>
                        @endif
                    </div>
                    <div class="tab-pane fade content-profil" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                        <div class="sub-title">Messages</div>
                        <div class="p-message">Pas encore disponible</div>
                    </div>
                    <div class="tab-pane fade content-profil" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                        <div class="sub-title">Paramètres</div>
                        <div class="l-user">Nom : {{Session()->get('user.lastName')}}</div>
                        <div class="l-user">Prénom : {{Session()->get('user.firstName')}}</div>
                        <div class="t-user">Pseudo : {{Session()->get('user.pseudo')}}</div>
                        <div class="t-user">Mail : {{Session()->get('user.email')}}</div></div>
                </div>
            </div>
        </div>

    @else
        <script>
            window.location.href = '{{url("connection")}}';
        </script>
    @endif
@endsection