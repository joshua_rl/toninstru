@extends('layouts.app')
@section('content')
    <div class="page-title">Offres</div>
    @if(count($deals) >= 1)
        @foreach($deals as $deal)
            <div class="add">
                <div class="title-add">{{$deal->titleDeals}}</div>
                <div class="price-add">{{$deal->priceDeals}} €</div>
                <div class="date-add">{{$deal->dateDeals}}</div>
                <div class="city-add">{{$deal->cityUser}} {{$deal->pcUser}}</div>
                <div class="instrument-add">Catégorie : {{$deal->wordingInstrument}}</div>
            </div>
        @endforeach
    @else
        <div>Aucune Offres existante !</div>
    @endif
@endsection