@extends('layouts.app')

@section('content')
    <div class="page-title">Demandes</div>
    @if(count($requests) >= 1)
        @foreach($requests as $request)
            <div class="add">
                <div class="title-add">{{$request->titleRequest}}</div>
                @if($request->priceRequest == null)
                    <div class="price-add">-- €</div>
                @else
                    <div class="price-add">{{$request->priceRequest}} €</div>
                @endif
                <div class="date-add">{{$request->dateRequest}}</div>
                <div class="city-add">{{$request->cityUser}} {{$request->pcUser}}</div>
                <div class="instrument-add">Catégorie : {{$request->wordingInstrument}}</div>
            </div>
        @endforeach
    @else
        <div>Aucune Demandes existante !</div>
    @endif

@endsection