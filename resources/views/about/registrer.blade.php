@extends('layouts.app')

@section('content')
    <div class="page-title">Inscription</div>
    <div class="c-registrer">

        {!! Form::open([ 'action'=>'UserController@registrerUser', 'method'=>'POST', 'class'=>'f-connection']) !!}
            <div class="row">
                <div class="col">
                    <!-- Last Name -->
                    {!! Form::label('l-lname', 'Nom*', array('class' => 'r-label')) !!}
                </div>
                <div class="col">
                    <!-- First Name -->
                    {!! Form::label('l-fname', 'Prénom*', array('class' => 'r-label')) !!}
                </div>

                <div class="w-100"></div>

                <div class="col">
                {!! Form::text('lastName',old('lastName'), array('class' => 'r-lname','placeholder'=>'Nom de Famille')) !!}

                <!-- Display error message -->
                    @if($errors->has('lastName'))
                        <div class="alert-danger">
                            <ul>
                                <li>{{$errors->first('lastName')}}</li>
                            </ul>
                        </div>
                    @endif
                </div>

                <div class="col">
                    {!! Form::text('firstName',old('firstName'), array('class' => 'r-fname','placeholder'=>'Prénom')) !!}

                    <!-- Display error message -->
                    @if($errors->has('firstName'))
                        <div class="alert-danger">
                            <ul>
                                <li>{{$errors->first('firstName')}}</li>
                            </ul>
                        </div>
                    @endif</div>


            </div>


            <!-- Pseudo -->
            {!! Form::label('l-pseudo', 'Pseudo*', array('class' => 'r-label')) !!}
            {!! Form::text('pseudo',old('pseudo'), array('class' => 'r-pseudo','placeholder'=>'Pseudo')) !!}

            @if($errors->has('pseudo'))
                <div class="alert-danger">
                    <ul>
                        <li>{{$errors->first('pseudo')}}</li>
                    </ul>
                </div>
            @endif

        <div class="row">
            <div class="col">
                <!-- City Location -->
                {!! Form::label('l-city', 'Ville*', array('class' => 'r-label')) !!}
            </div>
            <div class="col">
                <!-- Postal Code -->
                {!! Form::label('l-pc', 'Code Postal*', array('class' => 'r-label')) !!}
            </div>

            <div class="w-100"></div>

            <div class="col">
                {!! Form::text('city',old('city'), array('class' => 'r-city','placeholder'=>'Ville')) !!}

                <!-- Display error message -->
                    @if($errors->has('city'))
                        <div class="alert-danger">
                            <ul>
                                <li>{{$errors->first('city')}}</li>
                            </ul>
                        </div>
                    @endif
            </div>

            <div class="col">
                {!! Form::text('postalCode',old('postalCode'), array('class' => 'r-pc','placeholder'=>'Code postal')) !!}

                <!-- Display error message -->
                @if($errors->has('postalCode'))
                    <div class="alert-danger">
                        <ul>
                            <li>{{$errors->first('postalCode')}}</li>
                        </ul>
                    </div>
                @endif
            </div>

        </div>

            <!-- Telephone -->
            {!! Form::label('l-phone', 'Téléphone', array('class' => 'r-label')) !!}
            {!! Form::text('phone',old('phone'), array('class' => 'r-phone', 'placeholder'=>'Numéro de téléphone')) !!}

            <!-- Display error message -->
            @if($errors->has('phone'))
                <div class="alert-danger">
                    <ul>
                        <li>{{$errors->first('phone')}}</li>
                    </ul>
                </div>
            @endif

            <!-- E-mail -->
            {!! Form::label('l-email', 'Adresse e-mail*', array('class' => 'r-label')) !!}
            {!! Form::text('email',old('email'), array('class' => 'r-mail','placeholder'=>'xyz@example.com')) !!}

            @if($errors->has('email'))
                <div class="alert-danger">
                    <ul>
                        <li>{{$errors->first('email')}}</li>
                    </ul>
                </div>
            @endif

            <!-- Password -->
            {!! Form::label('l-password', 'Mot de passe*', array('class' => 'r-label')) !!}
            {!! Form::password('password', array('class' => 'r-pass' ,'placeholder'=>'Mot de passe', 'value'=> old('password'))) !!}

            <!-- Display error message -->
            @if($errors->has('password'))
                <div class="alert-danger">
                    <ul>
                        <li>{{$errors->first('password')}}</li>
                    </ul>
                </div>
            @endif

            <!-- Confirm Password -->
            {!! Form::label('l-password', 'Confirmer le mot de passe*', array('class' => 'r-label')) !!}
            {!! Form::password('password_confirmation', array('class' => 'r-pass','placeholder'=>'Confirmer votre mot de passe','value'=> old('password_confirmation'))) !!}

            <!-- Display error message -->
            @if($errors->has('password_confirmation'))
                <div class="alert-danger">
                    <ul>
                        <li>{{$errors->first('password_confirmation')}}</li>
                    </ul>
                </div>
            @endif

            <!-- Submit -->
            {!! Form::submit("S'inscrire", array('class' => 'r-sub')) !!}

        {!! Form::close() !!}


    </div>
@endsection
