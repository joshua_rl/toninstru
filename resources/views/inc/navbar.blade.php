<!-- Implement nav bar -->
<ul class="nav">
    <li class="item"><a href="/deals">OFFRES</a></li>
    <li class="item"><a href="/requests">DEMANDES</a></li>
    <li class="item"><a href="/add">FAIRE UNE ANNONCE</a></li>
    <li class="item"><a href="/profil">PROFIL</a></li>
    <li class="f-item">
        <!--<form id="f-search" class="f-search">-->
        {!! Form::open([ 'action'=> '', 'url' => '/', 'method' => 'POST', 'class'=>'f-search', 'id' => 'f-search']) !!}
            <input class="search" type="text" placeholder="Recherche rapide...">
            <i class='fas fa-search i-nav'></i>
        {!!  Form::close()!!}
        <!--</form>-->

    </li>
    <!--
    -->
</ul>