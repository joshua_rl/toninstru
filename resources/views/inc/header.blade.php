
@if(Session::has('user'))
    <!-- Replace login button by logout button -->
    <div id="logout" class="logon" onclick="location.href='{{ url('logout') }}'"><i class="fas fa-user"></i> SE DÉCONNECTER</div>

@else
    <!-- Implement Modal-->
    <div class="logon" data-toggle="modal" data-target="#Modal"><i class="fas fa-user"></i> SE CONNECTER</div>
    <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalLabel">Se Connecter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- Modal Form -->
                    {!! Form::open(['action'=>'UserController@store', 'method'=>'POST', 'class'=>'f-connection']) !!}
                        {!! Form::label('l-email', 'Adresse e-mail', array('class' => 'f-label')) !!}
                        {!! Form::text('email','', array('class' => 'con-mail','placeholder'=>'xyz@example.com')) !!}<br />
                        {!! Form::label('l-password', 'Mot de passe', array('class' => 'f-label')) !!}
                        {!! Form::password('password', array('class' => 'con-pass')) !!}<br />
                        {!! Form::submit('Connexion', array('class' => 'con-sub')) !!}
                        <a class="btn-sub" href="{{ url('registrer') }}">S'inscrire</a>
                    {!!  Form::close()!!}
                </div>
                <div class="modal-footer">
                    <div class="close-modal" data-dismiss="modal">Fermer</div>
                </div>
            </div>
        </div>
    </div>
@endif


<!-- Implement Carousel -->
<div id="carousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="{{ asset('img/b_guitar.jpg') }}" alt="First slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{asset('img/b_drum.jpg') }}" alt="Second slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('img/b_piano.jpg') }}" alt="Third slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('img/b_bass.jpg') }}" alt="Fourth slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('img/b_micro.jpg') }}" alt="Fift slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('img/b_flute.jpg') }}" alt="Sixth slide">
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('img/b_studio.jpg') }}" alt="Seventh slide">
        </div>
    </div>
</div>